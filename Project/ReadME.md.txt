1. After moving into the data folder, load the data.json file by command "json-server data.json". By default JSON server will run on port 3000.
2. After moving into the Project folder using "cd project" command, use "live-server" command to run the application. By default my application will run on 8080 port.
3. Please hover over poster or title of movie to see the more details of the movie.
4. Some posters are not loaded from the url. Please ignore them.
5. Click on add to favourite button to add.
6. Click on delete from favourite button to delete.
